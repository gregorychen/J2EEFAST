/*
 * All content copyright http://www.j2eefast.com, unless 
 * otherwise indicated. All rights reserved.
 * No deletion without permission
 */
package com.j2eefast.modules.demo.contorller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Validator;
import com.google.common.collect.Lists;
import com.j2eefast.common.core.controller.BaseController;
import com.j2eefast.common.core.redis.annotaion.aop.DistributedLock;
import com.j2eefast.common.core.utils.RedisUtil;
import com.j2eefast.common.core.utils.ResponseData;
import com.j2eefast.common.core.utils.ToolUtil;
import com.j2eefast.framework.utils.Constant;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 前端演示Demo
 * @author huanzhou
 * @Date 2020-06-19
 */
@Controller
@RequestMapping("/sys/demo")
public class SysDemoController extends BaseController {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedisUtil redisUtil;
    private String urlPrefix = "modules/sys/demo";

    /**
     * 前端模板引擎说明
     * @param mmap
     * @return
     */
    @RequiresPermissions("sys:demo:mode")
    @GetMapping("/mode")
    public String mode(ModelMap mmap) {
        mmap.put("mode",FileUtil.readUtf8String(Constant.BASE_WEB_HTML +urlPrefix+ "/mode.txt"));
        return urlPrefix + "/mode";
    }

    /**
     * 栅格布局案例
     * @param mmap
     * @return
     */
    @GetMapping("/layoutForm")
    public String layoutForm(ModelMap mmap) {
        return urlPrefix + "/layoutForm";
    }


    @RequiresPermissions("sys:demo:component")
    @GetMapping("/component")
    public String component() {
        return urlPrefix + "/component";
    }

    @GetMapping("/table")
    public String table(){
        return urlPrefix + "/table";
    }

    /**
     * 卡片表格
     * @author ZhouZhou
     * @date 2022-04-29 15:58
     * @return
     */
    @GetMapping("/cardTable")
    public String cardTable(){
        return urlPrefix + "/cardTable";
    }
    
    
    @GetMapping("/fromTable")
    public String fromTable(){
        return urlPrefix + "/fromTable";
    }
    
    @GetMapping("/enc")
    public String enc(){
        return urlPrefix + "/enc";
    }
    
    /**
     * 创建表单视图
     * @return
     */
    @RequiresPermissions("sys:demo:addfrom")
    @GetMapping("/addfrom")
    public String addfrom() {
        return urlPrefix + "/addfrom";
    }

    /**
     * 图表示例
     * @return
     */
    @RequiresPermissions("sys:demo:addrpert")
    @GetMapping("/addrpert")
    public String addrpert() {
        return urlPrefix + "/addrpert";
    }
    
    
    /**
     * 多表联动示例
     */
    @RequiresPermissions("sys:demo:multiTable")
    @GetMapping("/multiTable")
    public String multiTable() {
        return urlPrefix + "/multiTable";
    }
    
    /**
     * 消息组件
     * @return
     */
    @RequiresPermissions("sys:demo:msg")
    @GetMapping("/msg")
    public String msg() {
        return urlPrefix + "/msg";
    }


    @ResponseBody
    @RequestMapping(value = "/testSendEmail", method = RequestMethod.POST)
    public ResponseData testSendEmail(String email) {
        if(!Validator.isEmail(email)){
            return ResponseData.error("请输入正确邮箱地址!");
        }
        if(!redisUtil.setIfAbsent("demo:test:"+email,email,60*30)){
            return ResponseData.error("您的邮箱已经发送了,请不要重复发送!");
        }
        //测试通过报表发送邮件、可以根据自身业务修改
        Map<String,Object> parameters=new HashMap<String,Object>();
        List<String> tos = Lists.newArrayList();
        tos.add(email);
        ToolUtil.sendMailJu(tos,null,null,"2022-05-01 对账情况","jdgyby-pMK3yxqgzeIeMN",parameters);
        return ResponseData.success();
    }

    /**
     * 表单导向案例
     * @return
     */
    @RequiresPermissions("sys:demo:wizard")
    @GetMapping("/wizard")
    public String wizard(ModelMap mmap) {

        /** 测试数据*/
        List<Map<String,String>> listMap = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("key","default");//, arrows, dots, progress
        map.put("value","default");
        listMap.add(map);
        Map<String, String> map1 = new HashMap<>();
        map1.put("key","arrows");//, arrows, dots, progress
        map1.put("value","arrows");
        listMap.add(map1);
        Map<String, String> map2 = new HashMap<>();
        map2.put("key","dots");//, arrows, dots, progress
        map2.put("value","dots");
        listMap.add(map2);
        Map<String, String> map3 = new HashMap<>();
        map3.put("key","progress");//, arrows, dots, progress
        map3.put("value","progress");
        listMap.add(map3);
        mmap.put("themeLists",listMap);

        List<Map<String,String>> listMap2 = new ArrayList<>();
        Map<String, String> maap = new HashMap<>();
        maap.put("key","none");
        maap.put("value","None");
        listMap2.add(maap);
        Map<String, String> maap1 = new HashMap<>();
        maap1.put("key","fade");
        maap1.put("value","Fade");
        listMap2.add(maap1);
        Map<String, String> maap2 = new HashMap<>();
        maap2.put("key","slide-horizontal");
        maap2.put("value","Slide Horizontal");
        listMap2.add(maap2);
        Map<String, String> maap3 = new HashMap<>();
        maap3.put("key","slide-vertical");
        maap3.put("value","Slide Vertical");
        listMap2.add(maap3);
        Map<String, String> maap4 = new HashMap<>();
        maap4.put("key","slide-swing");
        maap4.put("value","Slide Swing");
        listMap2.add(maap4);
        mmap.put("animationLists",listMap2);


        List<Map<String,String>> listMap3 = new ArrayList<>();
        Map<String, String> maasp1 = new HashMap<>();
        maasp1.put("key","1");
        maasp1.put("value","1");
        listMap3.add(maasp1);
        Map<String, String> maasp2 = new HashMap<>();
        maasp2.put("key","2");
        maasp2.put("value","2");
        listMap3.add(maasp2);
        Map<String, String> maasp3 = new HashMap<>();
        maasp3.put("key","3");
        maasp3.put("value","3");
        listMap3.add(maasp3);
        Map<String, String> maasp4 = new HashMap<>();
        maasp4.put("key","4");
        maasp4.put("value","4");
        listMap3.add(maasp4);
        mmap.put("stepLists",listMap3);

        List<Map<String,String>> listMap4 = new ArrayList<>();
        Map<String, String> maamp1 = new HashMap<>();
        maamp1.put("key","751312449@qq.com");
        maamp1.put("value","751312449@qq.com");
        listMap4.add(maamp1);
        Map<String, String> maamp2 = new HashMap<>();
        maamp2.put("key","534354354334543");
        maamp2.put("value","534354354334543");
        listMap4.add(maasp2);
        mmap.put("cardLists",listMap4);
        return urlPrefix + "/wizard";
    }


    @RequestMapping(value = "/echarts1")
    @ResponseBody
    public ResponseData echarts1(){
        List<Map<String,Object>> ref = new ArrayList<>();
        Map<String,Object> map0 = new HashMap<>();
        map0.put("name","星期一");
        map0.put("value","123");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期二");
        map0.put("value","80");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期三");
        map0.put("value","180");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期四");
        map0.put("value","90");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期五");
        map0.put("value","110");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期六");
        map0.put("value","30");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期日");
        map0.put("value","130");
        ref.add(map0);
        return success(ref);
    }

    @RequestMapping(value = "/echarts")
    @ResponseBody
    public ResponseData echarts(){
        List<Map<String,Object>> ref = new ArrayList<>();
        Map<String,Object> map0 = new HashMap<>();
        map0.put("name","星期一");
        map0.put("value","123");
        map0.put("type","微信");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期二");
        map0.put("value","80");
        map0.put("type","微信");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期三");
        map0.put("value","180");
        map0.put("type","微信");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期四");
        map0.put("value","90");
        map0.put("type","微信");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期五");
        map0.put("value","110");
        map0.put("type","微信");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期六");
        map0.put("value","30");
        map0.put("type","微信");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期日");
        map0.put("value","130");
        map0.put("type","微信");
        ref.add(map0);

        map0 = new HashMap<>();
        map0.put("name","星期一");
        map0.put("value","30");
        map0.put("type","支付宝");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期二");
        map0.put("value","110");
        map0.put("type","支付宝");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期三");
        map0.put("value","150");
        map0.put("type","支付宝");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期四");
        map0.put("value","67");
        map0.put("type","支付宝");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期五");
        map0.put("value","120");
        map0.put("type","支付宝");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期六");
        map0.put("value","45");
        map0.put("type","支付宝");
        ref.add(map0);
        map0 = new HashMap<>();
        map0.put("name","星期日");
        map0.put("value","170");
        map0.put("type","支付宝");
        ref.add(map0);
        return success(ref);
    }

    @GetMapping("/test")
    public String test(ModelMap mmap) {
        mmap.put("mode",FileUtil.readUtf8String(Constant.BASE_WEB_HTML +urlPrefix+ "/mode1.txt"));
        return urlPrefix + "/test";
    }

    @GetMapping("/test1")
    public String test1(ModelMap mmap) {
        return urlPrefix + "/test1";
    }



////    获取redis信息
//    @RequestMapping(value = "/getRedis", method = RequestMethod.GET)
//    @ResponseBody
//    public ResponseData getRedis() {
//        Properties info = (Properties) redisTemplate.execute((RedisCallback) redisConnection -> redisConnection.info());
//        Properties commandStats = (Properties) redisTemplate.execute((RedisCallback) redisConnection -> redisConnection.info("commandstats"));
//        Object dbSize = redisTemplate.execute((RedisCallback) redisConnection -> redisConnection.dbSize());
//        Map<String, Object> result = new HashMap<>(4);
//        result.put("info", info);
//        result.put("dbSize", dbSize);
//        result.put("commandStats", commandStats);
//        result.put("time", DateUtil.format(new Date(), DatePattern.NORM_TIME_PATTERN));
//        return success(result);
//    }

}
